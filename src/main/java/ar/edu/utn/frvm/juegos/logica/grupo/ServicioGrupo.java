package ar.edu.utn.frvm.juegos.logica.grupo;

import logisoft.generico.logica.ServicioGenerico;

import org.springframework.stereotype.Service;

import ar.edu.utn.frvm.juegos.dominio.grupo.Grupo;

@Service
public class ServicioGrupo  extends ServicioGenerico<Grupo, Long> {

}
