package ar.edu.utn.frvm.juegos.dominio.partido;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoVoley;
import lombok.Data;

@Data
@Entity
public class PartidoVoley extends Partido {

	private int tantosLocal1;
	private int tantosLocal2;
	private int tantosLocal3;
	private int tantosVisita1;
	private int tantosVisita2;
	private int tantosVisita3;

	public int getSetsLocal() {
		int sets = 0;
		if (tantosLocal1 >= 25 && (tantosLocal1-tantosVisita1) >= 2) {
			sets++;
		}
		if (tantosLocal2 >= 25 && (tantosLocal2-tantosVisita2) >= 2) {
			sets++;
		}
		if (tantosLocal3 >= 15 && (tantosLocal3-tantosVisita3) >= 2) {
			sets++;
		}
		return sets;
	}
	
	public int getSetsVisita() {
		int sets = 0;
		if (tantosVisita1 >= 25 && (tantosVisita1-tantosLocal1) >= 2) {
			sets++;
		}
		if (tantosVisita2 >= 25 && (tantosVisita2-tantosLocal2) >= 2) {
			sets++;
		}
		if (tantosVisita3 >= 15 && (tantosVisita3-tantosLocal3) >= 2) {
			sets++;
		}
		return sets;
	}
}
