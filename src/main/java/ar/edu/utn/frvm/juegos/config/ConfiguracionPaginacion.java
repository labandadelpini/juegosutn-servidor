package ar.edu.utn.frvm.juegos.config;

import logisoft.generico.logica.configuracion.ServicioPaginacion;
import logisoft.generico.web.configuracion.ControladorPaginacion;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfiguracionPaginacion {
	
	@Bean
	public ServicioPaginacion servicio() {
		return new ServicioPaginacion();
	}
	
//	@Bean
//	public ControladorPaginacion controlador() {
//		return new ControladorPaginacion();
//	}

}