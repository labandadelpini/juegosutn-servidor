package ar.edu.utn.frvm.juegos.config;

import logisoft.generico.logica.seguridad.ServicioPermisosIniciales;
import logisoft.generico.seguridad.metodos.ManejadorExpresiones;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

//@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
public class ConfiguracionSeguridadMetodos extends GlobalMethodSecurityConfiguration {
//	
//	@Autowired
//	private ServicioPermisosIniciales servicioPermisosIniciales;
//
//	@Override
//	protected MethodSecurityExpressionHandler createExpressionHandler() {
//		return new ManejadorExpresiones(servicioPermisosIniciales);
//	}

}

