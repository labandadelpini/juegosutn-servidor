package ar.edu.utn.frvm.juegos.dominio.partido;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoFutbol;
import lombok.Data;

@Data
@Entity
public class PartidoFutbol extends Partido {

	private int tantosLocal;
	private int tantosVisita;

	private boolean penales;
	private int penalesLocal;
	private int penalesVisita;


}
