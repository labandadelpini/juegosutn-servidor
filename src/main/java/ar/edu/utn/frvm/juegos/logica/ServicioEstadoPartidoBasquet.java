package ar.edu.utn.frvm.juegos.logica;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import logisoft.generico.logica.ServicioGenerico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoBasquet;
import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoFutbol;
import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoVoley;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioEstadoPartidoBasquet;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioEstadoPartidoVoley;

@Service
public class ServicioEstadoPartidoBasquet extends ServicioGenerico<EstadoPartidoBasquet, Long>{
	
	@Autowired
	private RepositorioEstadoPartidoBasquet repositorio;

	@PostConstruct
	private void crearEstados() {
		Set<EstadoPartidoBasquet> empresas = new HashSet<EstadoPartidoBasquet>();
		empresas.add(new EstadoPartidoBasquet("Programado", 10, "P", false));
		empresas.add(new EstadoPartidoBasquet("En Juego - Primer Cuarto", 20, "1er C", true));
		empresas.add(new EstadoPartidoBasquet("En Juego - Segundo Cuarto", 30, "2do C", true));
		empresas.add(new EstadoPartidoBasquet("En Juego - Suplementario", 40, "Sup", true));
		empresas.add(new EstadoPartidoBasquet("Finalizado", 50, "F", false));
		empresas.add(new EstadoPartidoBasquet("Cancelado", 60, "C", false));
		for (EstadoPartidoBasquet f : empresas) {
			if (repositorio.findByNombre(f.getNombre()) == null) {
				repositorio.save(f);
			}
		}
	}

}
