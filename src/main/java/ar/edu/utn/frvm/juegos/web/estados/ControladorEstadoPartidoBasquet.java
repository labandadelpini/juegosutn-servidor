package ar.edu.utn.frvm.juegos.web.estados;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoBasquet;
import logisoft.generico.web.genericos.ControladorGenerico;

@RestController
@RequestMapping("/estados/basquet")
public class ControladorEstadoPartidoBasquet extends
		ControladorGenerico<EstadoPartidoBasquet, Long> {

}
