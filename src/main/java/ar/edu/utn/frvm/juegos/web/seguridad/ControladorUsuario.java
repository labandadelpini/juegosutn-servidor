package ar.edu.utn.frvm.juegos.web.seguridad;

import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import logisoft.generico.dominio.seguridad.Usuario;
import logisoft.generico.web.genericos.ControladorGenericoActualizar;

@RestController
@RequestMapping("/usuarios")
public class ControladorUsuario extends ControladorGenericoActualizar<Usuario, Long> {
	
	@RequestMapping(value="/actual",method=RequestMethod.GET)
	public Usuario getUsuarioActual(@AuthenticationPrincipal Usuario usuario) {
		return usuario;
	}

}
