package ar.edu.utn.frvm.juegos.persistencia;

import logisoft.generico.persistencia.RepositorioGenerico;

import org.springframework.stereotype.Repository;

import ar.edu.utn.frvm.juegos.dominio.partido.PartidoFutbol;
import ar.edu.utn.frvm.juegos.dominio.partido.PartidoVoley;

@Repository
public interface RepositorioPartidoVoley extends
		RepositorioGenerico<PartidoVoley, Long> {

}
