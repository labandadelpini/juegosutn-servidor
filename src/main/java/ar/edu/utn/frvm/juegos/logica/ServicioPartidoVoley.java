package ar.edu.utn.frvm.juegos.logica;

import java.util.Date;

import logisoft.generico.logica.ServicioGenerico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.utn.frvm.juegos.dominio.estado.HistorialEstado;
import ar.edu.utn.frvm.juegos.dominio.partido.PartidoFutbol;
import ar.edu.utn.frvm.juegos.dominio.partido.PartidoVoley;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioEstadoPartidoFutbol;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioEstadoPartidoVoley;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioPartidoFutbol;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioPartidoVoley;

@Service
public class ServicioPartidoVoley extends
		ServicioGenerico<PartidoVoley, Long> {
	
	@Autowired
	private RepositorioPartidoVoley repositorio;
	
	@Autowired
	private RepositorioEstadoPartidoVoley repositorioEstado;
	
	public PartidoVoley iniciar(Long id) {
		PartidoVoley partido = repositorio.findOne(id);
		Date fecha = new Date();
		HistorialEstado nuevo = new HistorialEstado();
		nuevo.setEstado(repositorioEstado.findByNombre("En Juego - Primer Set"));
		nuevo.setFecha(fecha);
		partido.getEstados().add(nuevo);
		partido.setEstado(repositorioEstado.findByNombre("En Juego - Primer Set"));
		partido.setFechaInicio(fecha);
		partido.setUltimaActulizacion(fecha);
		return repositorio.save(partido);
	}
	
	@Transactional
	protected PartidoVoley guardarImp(PartidoVoley objeto) {
		Date fecha = new Date();
		HistorialEstado nuevo = new HistorialEstado();
		nuevo.setEstado(repositorioEstado.findByNombre("Programado"));
		nuevo.setFecha(fecha);
		objeto.getEstados().add(nuevo);
		objeto.setEstado(repositorioEstado.findByNombre("Programado"));
		objeto.setUltimaActulizacion(fecha);
		return repositorio.save(objeto);
	}
	
	@Transactional
	protected PartidoVoley actualizarItemImp(PartidoVoley objeto) {
		PartidoVoley objetoDB =  repositorio.findOne(objeto.getId());
		Date fecha = new Date();
		if (objetoDB.getEstado().getId() == objeto.getEstado().getId()) { //Mismo Estado
			
		}else {
			HistorialEstado nuevo = new HistorialEstado();
			nuevo.setEstado(objeto.getEstado());
			nuevo.setFecha(fecha);
			objeto.getEstados().add(nuevo);
		}
		objeto.setUltimaActulizacion(fecha);
		return repositorio.save(objeto);
	}

}
