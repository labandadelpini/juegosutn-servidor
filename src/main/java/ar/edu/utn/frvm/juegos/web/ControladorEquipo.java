package ar.edu.utn.frvm.juegos.web;

import logisoft.generico.web.genericos.ControladorGenericoActualizar;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.utn.frvm.juegos.dominio.Equipo;

@RestController
@RequestMapping("/equipos")
public class ControladorEquipo extends ControladorGenericoActualizar<Equipo, Long>{

}
