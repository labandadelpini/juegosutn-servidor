package ar.edu.utn.frvm.juegos.logica.grupo;

import logisoft.generico.logica.ServicioGenerico;

import org.springframework.stereotype.Service;

import ar.edu.utn.frvm.juegos.dominio.Campeonato;

@Service
public class ServicioCampeonato  extends ServicioGenerico<Campeonato, Long> {

}
