package ar.edu.utn.frvm.juegos.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import logisoft.generico.logica.ServicioImagen;
import logisoft.generico.logica.reporte.ServicioReporte;
import logisoft.generico.logica.reporte.ServicioReporteGenerico;
import logisoft.generico.util.ConstructorReporte;
import logisoft.generico.web.soporte.ControladorReportes;

@Configuration
public class ConfiguracionReporte {
	

	@Bean
	public ControladorReportes controladorReportes() {
		return new ControladorReportes();
	}
	
	@Bean
	public ServicioReporte servicioReporte() {
		return new ServicioReporte();
	}
	
	@Bean
	public ConstructorReporte constructorReporte(@Value("classpath:imagenes/logo.png") Resource logo) {
		return new ConstructorReporte(logo);
	}
	
	@Bean
	public ServicioReporteGenerico servicioReporteGenerico(@Value("classpath:reportes/reporteGenerico.jrxml") Resource reporteBase) {
		return new ServicioReporteGenerico(reporteBase);
	}
	
	@Bean
	public ServicioImagen servicio() {
		return new ServicioImagen();
	}


}
