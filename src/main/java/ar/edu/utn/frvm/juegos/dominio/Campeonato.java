package ar.edu.utn.frvm.juegos.dominio;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import logisoft.generico.dominio.ObjetoDominio;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Campeonato extends ObjetoDominio{
	@NonNull
	private String nombre;

}
