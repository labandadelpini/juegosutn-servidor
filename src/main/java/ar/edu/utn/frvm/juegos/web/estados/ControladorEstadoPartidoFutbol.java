package ar.edu.utn.frvm.juegos.web.estados;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoFutbol;
import logisoft.generico.web.genericos.ControladorGenerico;

@RestController
@RequestMapping("/estados/futbol")
public class ControladorEstadoPartidoFutbol extends
		ControladorGenerico<EstadoPartidoFutbol, Long> {

}
