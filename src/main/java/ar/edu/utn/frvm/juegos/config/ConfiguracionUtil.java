package ar.edu.utn.frvm.juegos.config;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.annotation.PostConstruct;

import logisoft.generico.dominio.ObjetoAuditable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.xebia.jacksonlombok.JacksonLombokAnnotationIntrospector;

@Configuration
@EnableJpaAuditing
public class ConfiguracionUtil {
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@PostConstruct
	public void configurarObjectMapper() {
		objectMapper.setConfig(objectMapper.getSerializationConfig().withView(
				ObjetoAuditable.Vistas.Defecto.class));
		objectMapper
				.setAnnotationIntrospector(new JacksonLombokAnnotationIntrospector());
	}
	
	@Bean
	public DateFormat simpleDateFormat() {
		return new SimpleDateFormat("yyyyMMdd");
	}

}
