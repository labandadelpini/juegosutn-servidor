package ar.edu.utn.frvm.juegos.dominio.estado;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import logisoft.generico.dominio.ObjetoDominio;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class HistorialEstado  extends ObjetoDominio{
	@NonNull
	@ManyToOne
	private EstadoPartido estado;
	@NonNull
	private Date fecha;

}
