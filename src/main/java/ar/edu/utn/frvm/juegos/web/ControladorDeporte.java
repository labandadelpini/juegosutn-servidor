package ar.edu.utn.frvm.juegos.web;

import logisoft.generico.web.genericos.ControladorGenericoActualizar;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.utn.frvm.juegos.dominio.partido.Deporte;

@RestController
@RequestMapping("/deportes")
public class ControladorDeporte extends ControladorGenericoActualizar<Deporte, Long>{

}
