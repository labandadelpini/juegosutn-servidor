package ar.edu.utn.frvm.juegos.persistencia;

import org.springframework.stereotype.Repository;

import logisoft.generico.persistencia.RepositorioGenerico;
import ar.edu.utn.frvm.juegos.dominio.partido.Deporte;

@Repository
public interface RepositorioDeporte  extends RepositorioGenerico<Deporte, Long> {
	
	public Deporte findByNombre(String nombre);

}
