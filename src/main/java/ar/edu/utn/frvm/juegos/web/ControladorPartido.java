package ar.edu.utn.frvm.juegos.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.common.collect.Lists;

import ar.edu.utn.frvm.juegos.dominio.partido.Partido;
import ar.edu.utn.frvm.juegos.logica.ServicioPartido;
import ar.edu.utn.frvm.juegos.logica.ServicioPartidoBasquet;
import ar.edu.utn.frvm.juegos.logica.ServicioPartidoFutbol;
import ar.edu.utn.frvm.juegos.logica.ServicioPartidoVoley;

@RestController
@RequestMapping("/partidos")
public class ControladorPartido {

	@Autowired
	private ServicioPartido servicio;
	
	@Autowired
	private ControladorPartidoFutbol controladorFutbol;
	@Autowired
	private ControladorPartidoBasquet controladorBasquet;
	@Autowired
	private ControladorPartidoVoley controladorVoley;
	
	
	@JsonView({Partido.Vistas.Resumen.class})
	@RequestMapping(method = RequestMethod.GET)
	public List<Partido> getPartidos() {
		return servicio.getPartidos();
	}
	
	@JsonView({Partido.Vistas.Resumen.class})
	@RequestMapping(method = RequestMethod.GET, value = "/envivo")
	public List<Partido> getPartidosFiltradosYOrdenado(@RequestParam Map<String, String> parametros) {
		List<Partido> envivo = new ArrayList<Partido>();
		envivo.addAll(Lists.newArrayList(controladorFutbol.getPagina(parametros)));
		envivo.addAll(Lists.newArrayList(controladorBasquet.getPagina(parametros)));
		envivo.addAll(Lists.newArrayList(controladorVoley.getPagina(parametros)));
		return envivo;
	}
	
//	@JsonView({Partido.Vistas.Resumen.class})
//	@RequestMapping(method = RequestMethod.GET, value = "/envivo", params = { "sort" })
//	public List<Partido> getPartidosFiltrados(@RequestParam Map<String, String> parametros, Pageable pagina) {
//		parametros.remove("sort");
//		parametros.remove("size");
//		parametros.remove("page");
//		
//		List<Partido> envivo = new ArrayList<Partido>();
//		envivo.addAll(Lists.newArrayList(servicioFutbol.listarFiltradoYPaginado(parametros,pagina)));
//		envivo.addAll(Lists.newArrayList(servicioBasquet.listarFiltradoYPaginado(parametros,pagina)));
//		envivo.addAll(Lists.newArrayList(servicioVoley.listarFiltradoYPaginado(parametros,pagina)));
//		return envivo;
//	}

	
}
