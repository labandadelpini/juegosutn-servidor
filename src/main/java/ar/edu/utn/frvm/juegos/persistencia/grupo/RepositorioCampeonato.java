package ar.edu.utn.frvm.juegos.persistencia.grupo;

import logisoft.generico.persistencia.RepositorioGenerico;

import org.springframework.stereotype.Repository;

import ar.edu.utn.frvm.juegos.dominio.Campeonato;

@Repository
public interface RepositorioCampeonato extends RepositorioGenerico<Campeonato, Long> {

}
