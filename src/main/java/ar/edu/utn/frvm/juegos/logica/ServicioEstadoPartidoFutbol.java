package ar.edu.utn.frvm.juegos.logica;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import logisoft.generico.logica.ServicioGenerico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoFutbol;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioEstadoPartidoFutbol;

@Service
public class ServicioEstadoPartidoFutbol extends ServicioGenerico<EstadoPartidoFutbol, Long>{
	
	@Autowired
	private RepositorioEstadoPartidoFutbol repositorio;

	@PostConstruct
	private void crearEstados() {
		Set<EstadoPartidoFutbol> empresas = new HashSet<EstadoPartidoFutbol>();
		empresas.add(new EstadoPartidoFutbol("Programado",10, "P", false));
		empresas.add(new EstadoPartidoFutbol("En Juego - Primer Tiempo",20, "PT", true));
		empresas.add(new EstadoPartidoFutbol("En Juego - Entretiempo",30, "ET", true));
		empresas.add(new EstadoPartidoFutbol("En Juego - Segundo Tiempo",40, "ST", true));
		empresas.add(new EstadoPartidoFutbol("En Juego - Penales",50, "Penales", false));
		empresas.add(new EstadoPartidoFutbol("Finalizado",60, "F", false));
		empresas.add(new EstadoPartidoFutbol("Cancelado",70, "C", false));
		for (EstadoPartidoFutbol f : empresas) {
			if (repositorio.findByNombre(f.getNombre()) == null) {
				repositorio.save(f);
			}
		}
	}
}
