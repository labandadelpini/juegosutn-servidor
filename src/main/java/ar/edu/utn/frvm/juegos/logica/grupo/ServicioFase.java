package ar.edu.utn.frvm.juegos.logica.grupo;

import logisoft.generico.logica.ServicioGenerico;

import org.springframework.stereotype.Service;

import ar.edu.utn.frvm.juegos.dominio.Fase;

@Service
public class ServicioFase  extends ServicioGenerico<Fase, Long> {

}
