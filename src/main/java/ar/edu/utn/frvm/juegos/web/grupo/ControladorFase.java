package ar.edu.utn.frvm.juegos.web.grupo;

import logisoft.generico.web.genericos.ControladorGenericoActualizar;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.utn.frvm.juegos.dominio.Fase;

@RestController
@RequestMapping("/fases")
public class ControladorFase extends
		ControladorGenericoActualizar<Fase, Long> {

}
