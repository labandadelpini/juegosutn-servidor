package ar.edu.utn.frvm.juegos;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;

import logisoft.generico.dominio.ObjetoAuditable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xebia.jacksonlombok.JacksonLombokAnnotationIntrospector;

@Configuration
@EnableAutoConfiguration
@EnableAsync
@EnableJpaAuditing
@ComponentScan
public class Aplicacion extends SpringBootServletInitializer {
	
	@Autowired
	private ObjectMapper objectMapper;

	public static void main(String[] args) {
		SpringApplication.run(Aplicacion.class,args);
	}
	
	@PostConstruct
	public void configurarObjectMapper() {
		objectMapper.setConfig(objectMapper.getSerializationConfig().withView(ObjetoAuditable.Vistas.Defecto.class));
		objectMapper.setAnnotationIntrospector(new JacksonLombokAnnotationIntrospector());
	}

}
