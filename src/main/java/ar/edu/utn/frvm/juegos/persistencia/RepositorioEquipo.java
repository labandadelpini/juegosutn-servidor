package ar.edu.utn.frvm.juegos.persistencia;

import org.springframework.stereotype.Repository;

import ar.edu.utn.frvm.juegos.dominio.Equipo;
import logisoft.generico.persistencia.RepositorioGenerico;

@Repository
public interface RepositorioEquipo extends RepositorioGenerico<Equipo, Long> {
	
	public Equipo findByNombre(String nombre);

}
