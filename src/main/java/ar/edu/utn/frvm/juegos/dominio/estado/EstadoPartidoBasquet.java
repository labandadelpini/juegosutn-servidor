package ar.edu.utn.frvm.juegos.dominio.estado;

import javax.persistence.Entity;

import logisoft.generico.dominio.ObjetoDominio;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class EstadoPartidoBasquet extends EstadoPartido {
	
	public EstadoPartidoBasquet(String nombre, int orden) {
		super(nombre, orden);
	}
	
	public EstadoPartidoBasquet(String nombre, int orden, String abreviatura, boolean tiempo) {
		super(nombre, orden, abreviatura, tiempo);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + orden;
		return result;
	}

}
