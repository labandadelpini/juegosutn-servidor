package ar.edu.utn.frvm.juegos.dominio.grupo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import ar.edu.utn.frvm.juegos.dominio.Equipo;
import ar.edu.utn.frvm.juegos.dominio.partido.Deporte;
import ar.edu.utn.frvm.juegos.dominio.partido.Partido;

import com.fasterxml.jackson.annotation.JsonView;

import logisoft.generico.dominio.ObjetoDominio;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Grupo extends ObjetoDominio {

	@NonNull
	private String nombre;
	private int orden;
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<Equipo> equipos = new HashSet<Equipo>();
	@JsonView({ Partido.Vistas.Completa.class })
	@ManyToMany(mappedBy="grupoLocal", fetch = FetchType.EAGER)
	private Set<Partido> partidosLocal = new HashSet<Partido>();
	@JsonView({ Partido.Vistas.Completa.class })
	@ManyToMany(mappedBy="grupoVisita", fetch = FetchType.EAGER)
	private Set<Partido> partidosVisita = new HashSet<Partido>();
	@ManyToOne
	private Deporte deporte;
	
}
