package ar.edu.utn.frvm.juegos.web.grupo;

import java.util.Map;

import logisoft.generico.web.genericos.ControladorGenericoActualizar;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import ar.edu.utn.frvm.juegos.dominio.Equipo;
import ar.edu.utn.frvm.juegos.dominio.grupo.Grupo;
import ar.edu.utn.frvm.juegos.dominio.partido.Partido;

@RestController
@RequestMapping("/grupos")
public class ControladorGrupo extends
		ControladorGenericoActualizar<Grupo, Long> {
	
	@Override
	@JsonView({Partido.Vistas.Completa.class})
	@RequestMapping("/{id}")
	public Grupo get(@PathVariable("id") Long id) {
		return servicioGenerico.getItem(id);
	}
	
	@RequestMapping("/{id}/equipos")
	public Iterable<Equipo> getEquipos(@PathVariable("id") Long id) {
		return servicioGenerico.getItem(id).getEquipos();
	}
	
	
	@Override
	@JsonView({Partido.Vistas.Resumen.class})
	@RequestMapping(params = { "sort" })
	public Iterable<Grupo> getPagina(@RequestParam Map<String, String> parametros,
			Pageable pagina) {
		return super.getPagina(parametros, pagina);
	}

}
