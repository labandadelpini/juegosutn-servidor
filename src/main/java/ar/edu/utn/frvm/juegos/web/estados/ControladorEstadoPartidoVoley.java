package ar.edu.utn.frvm.juegos.web.estados;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoFutbol;
import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoVoley;
import logisoft.generico.web.genericos.ControladorGenerico;
import logisoft.generico.web.genericos.ControladorGenericoActualizar;

@RestController
@RequestMapping("/estados/voley")
public class ControladorEstadoPartidoVoley extends
		ControladorGenerico<EstadoPartidoVoley, Long> {

}
