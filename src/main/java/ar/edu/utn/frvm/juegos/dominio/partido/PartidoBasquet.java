package ar.edu.utn.frvm.juegos.dominio.partido;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoBasquet;
import lombok.Data;

@Data
@Entity
public class PartidoBasquet extends Partido {
	private int tantosLocal;
	private int tantosVisita;
}
