package ar.edu.utn.frvm.juegos.logica;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.utn.frvm.juegos.dominio.partido.Partido;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioPartidoBasquet;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioPartidoFutbol;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioPartidoVoley;

@Service
public class ServicioPartido {

	@Autowired
	private RepositorioPartidoFutbol repositorioFutbol;
	@Autowired
	private RepositorioPartidoBasquet repositorioBasquet;
	@Autowired
	private RepositorioPartidoVoley repositorioVoley;

	public List<Partido> getPartidos() {
		List<Partido> partidos = new ArrayList<Partido>();
		partidos.addAll((Collection<? extends Partido>) repositorioFutbol
				.findAll());
		partidos.addAll((Collection<? extends Partido>) repositorioBasquet
				.findAll());
		partidos.addAll((Collection<? extends Partido>) repositorioVoley
				.findAll());
		return partidos;
	}
}
