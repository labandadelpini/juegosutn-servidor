package ar.edu.utn.frvm.juegos.logica;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import logisoft.generico.logica.ServicioGenerico;
import ar.edu.utn.frvm.juegos.dominio.Equipo;
import ar.edu.utn.frvm.juegos.dominio.partido.Deporte;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioDeporte;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioEquipo;

@Service
public class ServicioEquipo extends ServicioGenerico<Equipo, Long> {
	
	@Autowired
	private RepositorioEquipo repositorio;
	
	@PostConstruct
	private void crearDeportes() {
		Set<Equipo> empresas = new HashSet<Equipo>();
		empresas.add(new Equipo("FR Avellaneda", "FRA"));
		empresas.add(new Equipo("FR Bahía Blanca", "FRBB"));
		empresas.add(new Equipo("FR Buenos Aires", "FRBA"));
		empresas.add(new Equipo("FR Chubut", "FRCH"));
		empresas.add(new Equipo("FR Concepción del Uruguay", "FRCU"));
		empresas.add(new Equipo("FR Concordia", "FRCON"));
		empresas.add(new Equipo("FR Córdoba", "FRC"));
		empresas.add(new Equipo("FR Delta", "FRD"));
		empresas.add(new Equipo("FR General Pacheco", "FRGP"));
		empresas.add(new Equipo("FR Buenos Aires", "FRBA"));
		empresas.add(new Equipo("FR Haedo", "FRH"));
		empresas.add(new Equipo("FR La Plata", "FRLP"));
		empresas.add(new Equipo("FR La Rioja", "FRLR"));
		empresas.add(new Equipo("FR Mendoza", "FRM"));
		empresas.add(new Equipo("FR del Neuquen", "FRN"));
		empresas.add(new Equipo("FR Paraná", "FRP"));
		empresas.add(new Equipo("FR Rafaela", "FRRA"));
		empresas.add(new Equipo("FR Resistencia", "FRRE"));
		empresas.add(new Equipo("FR Reconquista", "FRRQ"));
		empresas.add(new Equipo("FR Río Grande", "FRRG"));
		empresas.add(new Equipo("FR Rosario", "FRRO"));
		empresas.add(new Equipo("FR San Francisco", "FRSFCO"));
		empresas.add(new Equipo("FR San Nicolas", "FRSN"));
		empresas.add(new Equipo("FR San Rafael", "FRSR"));
		empresas.add(new Equipo("FR Santa Cruz", "FRSC"));
		empresas.add(new Equipo("FR Santa FE", "FRSF"));
		empresas.add(new Equipo("FR Trenque Lauquen", "FRTL"));
		empresas.add(new Equipo("FR Tucuman", "FRT"));
		empresas.add(new Equipo("FR Venado Tuerto", "FRVT"));
		empresas.add(new Equipo("FR Villa María", "FRVM"));
		empresas.add(new Equipo("UA Mar del Plata", "UAMDP"));
		
		for (Equipo f : empresas) {
			if (repositorio.findByNombre(f.getNombre()) == null) {
				repositorio.save(f);
			}
		}
	}

}
