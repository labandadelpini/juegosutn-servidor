package ar.edu.utn.frvm.juegos.logica;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import logisoft.generico.dominio.seguridad.EstadoUsuario;
import logisoft.generico.dominio.seguridad.Rol;
import logisoft.generico.dominio.seguridad.Usuario;
import logisoft.generico.logica.ServicioGenerico;
import logisoft.generico.persistencia.RepositorioRol;
import logisoft.generico.persistencia.RepositorioUsuario;

@Service
public class ServicioUsuario extends ServicioGenerico<Usuario, Long> {
	
	@Autowired
	private RepositorioRol repositorioRol;
	
	@Autowired
	private RepositorioUsuario repositorio;
	
	@Autowired
	private PasswordEncoder encoder;
	
	@Override
	public Usuario guardar(Usuario usuario) {
		usuario.setPassword(encoder.encode(usuario.getPassword()));
		return super.guardar(usuario);
	}
	
	public Usuario guardarUsuarioPostulante(Usuario usuario, String nombre, String apellido) {
		String nombreUsuario = nombre.substring(0, 1).toLowerCase() + apellido.toLowerCase();
		if (repositorio.findByNombre(nombreUsuario) != null) {
			int numero = 1;
			String nombreNuevo = nombreUsuario + numero;
			while (repositorio.findByNombre(nombreNuevo) != null) {
				numero++;
				nombreNuevo = nombreUsuario + numero;
			}
			nombreUsuario = nombreNuevo;
		}
		usuario.setNombre(nombreUsuario);
		Rol rol = repositorioRol.findByNombre("Postulante");
		usuario.getRoles().add(rol);
		usuario.setEstado(EstadoUsuario.SIN_CONFIRMAR);
		return guardar(usuario);
	}
	
	public Usuario guardarUsuarioIntermediario(Usuario usuario) {
		if (repositorio.findByNombre(usuario.getNombre()) != null) {
			throw new RuntimeException("El usuario ya existe");
		}
		Rol rol = repositorioRol.findByNombre("Intermediario");
		usuario.getRoles().add(rol);
		usuario.setEstado(EstadoUsuario.CONFIRMADO);
		return guardar(usuario);
	}

}
