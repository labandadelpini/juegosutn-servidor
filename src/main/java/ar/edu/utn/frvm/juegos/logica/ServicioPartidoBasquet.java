package ar.edu.utn.frvm.juegos.logica;

import java.util.Date;

import logisoft.generico.logica.ServicioGenerico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.utn.frvm.juegos.dominio.estado.HistorialEstado;
import ar.edu.utn.frvm.juegos.dominio.partido.PartidoBasquet;
import ar.edu.utn.frvm.juegos.dominio.partido.PartidoFutbol;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioEstadoPartidoBasquet;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioEstadoPartidoFutbol;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioPartidoBasquet;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioPartidoFutbol;

@Service
public class ServicioPartidoBasquet extends
		ServicioGenerico<PartidoBasquet, Long> {
	
	@Autowired
	private RepositorioPartidoBasquet repositorio;
	
	@Autowired
	private RepositorioEstadoPartidoBasquet repositorioEstado;

	public PartidoBasquet iniciar(Long id) {
		PartidoBasquet partido = repositorio.findOne(id);
		Date fecha = new Date();
		HistorialEstado nuevo = new HistorialEstado();
		nuevo.setEstado(repositorioEstado.findByNombre("En Juego - Primer Cuarto"));
		nuevo.setFecha(fecha);
		partido.getEstados().add(nuevo);
		partido.setEstado(repositorioEstado.findByNombre("En Juego - Primer Cuarto"));
		partido.setFechaInicio(fecha);
		partido.setUltimaActulizacion(fecha);
		return repositorio.save(partido);
	}
	
	@Transactional
	protected PartidoBasquet guardarImp(PartidoBasquet objeto) {
		Date fecha = new Date();
		HistorialEstado nuevo = new HistorialEstado();
		nuevo.setEstado(repositorioEstado.findByNombre("Programado"));
		nuevo.setFecha(fecha);
		objeto.getEstados().add(nuevo);
		objeto.setEstado(repositorioEstado.findByNombre("Programado"));
		objeto.setUltimaActulizacion(fecha);
		return repositorio.save(objeto);
	}
	
	@Transactional
	protected PartidoBasquet actualizarItemImp(PartidoBasquet objeto) {
		PartidoBasquet objetoDB =  repositorio.findOne(objeto.getId());
		Date fecha = new Date();
		if (objetoDB.getEstado().getId() == objeto.getEstado().getId()) { //Mismo Estado
			
		}else {
			HistorialEstado nuevo = new HistorialEstado();
			nuevo.setEstado(objeto.getEstado());
			nuevo.setFecha(fecha);
			objeto.getEstados().add(nuevo);
		}
		objeto.setUltimaActulizacion(fecha);
		return repositorio.save(objeto);
	}
	

}
