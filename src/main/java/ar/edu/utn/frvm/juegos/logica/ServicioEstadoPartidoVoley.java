package ar.edu.utn.frvm.juegos.logica;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import logisoft.generico.logica.ServicioGenerico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoFutbol;
import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoVoley;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioEstadoPartidoFutbol;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioEstadoPartidoVoley;

@Service
public class ServicioEstadoPartidoVoley extends
		ServicioGenerico<EstadoPartidoVoley, Long> {

	@Autowired
	private RepositorioEstadoPartidoVoley repositorio;

	@PostConstruct
	private void crearEstados() {
		Set<EstadoPartidoVoley> empresas = new HashSet<EstadoPartidoVoley>();
		empresas.add(new EstadoPartidoVoley("Programado", 10, "P", false));
		empresas.add(new EstadoPartidoVoley("En Juego - Primer Set", 20, "1er Set", false));
		empresas.add(new EstadoPartidoVoley("En Juego - Segundo Set", 30, "2do Set", false));
		empresas.add(new EstadoPartidoVoley("En Juego - Tercer Set", 40, "3er Set", false));
		empresas.add(new EstadoPartidoVoley("Finalizado", 50, "F", false));
		empresas.add(new EstadoPartidoVoley("Cancelado", 60, "C", false));
		for (EstadoPartidoVoley f : empresas) {
			if (repositorio.findByNombre(f.getNombre()) == null) {
				repositorio.save(f);
			}
		}
	}

}
