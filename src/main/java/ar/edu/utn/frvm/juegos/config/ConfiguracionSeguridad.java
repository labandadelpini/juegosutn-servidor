package ar.edu.utn.frvm.juegos.config;

import logisoft.generico.logica.seguridad.ServicioCreacionAdministrador;
import logisoft.generico.logica.seguridad.ServicioPermisosIniciales;
import logisoft.generico.persistencia.RepositorioPermiso;
import logisoft.generico.persistencia.RepositorioUsuario;
import logisoft.generico.seguridad.UserDetailsServiceAplicacion;
import logisoft.generico.seguridad.audit.SpringSecurityAuditorAware;
import logisoft.generico.seguridad.token.FiltroAutenticacionToken;
import logisoft.generico.seguridad.util.ConfiguracionHeadersCors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.header.writers.StaticHeadersWriter;

@Configuration
@EnableWebSecurity
public class ConfiguracionSeguridad extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsServiceAplicacion userDetails;

	// @Autowired
	// private ServicioAlmacenAutenticaciones servicioAlmacenAutenticaciones;

	public void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests()
				.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
				.antMatchers("/**").permitAll()
				.anyRequest().authenticated().and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
				.httpBasic();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth)
			throws Exception {
		auth.userDetailsService(userDetails).passwordEncoder(passwordEncoder());
		// auth.authenticationProvider(proveedorAutenticacionToken());

	}

	public FiltroAutenticacionToken filtroAutenticacionToken() throws Exception {
		return new FiltroAutenticacionToken(authenticationManager());
	}

	// public FiltroAsignacionToken filtroAsignacionToken() throws Exception {
	// return new FiltroAsignacionToken(servicioAlmacenAutenticaciones);
	// }
	//
	// @Bean
	// public ProveedorAutenticacionToken proveedorAutenticacionToken() {
	// return new ProveedorAutenticacionToken(servicioAlmacenAutenticaciones);
	// }

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public TextEncryptor textEncryptor() {
		return Encryptors
				.text("password", KeyGenerators.string().generateKey());
	}

	@Autowired
	@Bean
	public UserDetailsServiceAplicacion userDetailsServiceAplicacion(
			RepositorioUsuario repositorioUsuario) {
		return new UserDetailsServiceAplicacion(repositorioUsuario);
	}

	@Bean
	public ConfiguracionHeadersCors cors() {
		return new ConfiguracionHeadersCors();
	}

	
	@Bean
	public ServicioCreacionAdministrador servicioCrecionAdministrador() {
		return new ServicioCreacionAdministrador();
	}
	
	@Bean
	public SpringSecurityAuditorAware springSecurityAuditorAware() {
		return new SpringSecurityAuditorAware();
	}
	
	@Bean
	public ServicioPermisosIniciales servicioPermisosIniciales(RepositorioPermiso repositorio) {
		return new ServicioPermisosIniciales(repositorio);
	}
	
}