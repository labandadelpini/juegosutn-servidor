package ar.edu.utn.frvm.juegos.web;

import java.util.Map;

import logisoft.generico.web.genericos.ControladorGenericoActualizar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import ar.edu.utn.frvm.juegos.dominio.partido.Partido;
import ar.edu.utn.frvm.juegos.dominio.partido.PartidoBasquet;
import ar.edu.utn.frvm.juegos.dominio.partido.PartidoFutbol;
import ar.edu.utn.frvm.juegos.dominio.partido.PartidoVoley;
import ar.edu.utn.frvm.juegos.logica.ServicioPartidoFutbol;
import ar.edu.utn.frvm.juegos.logica.ServicioPartidoVoley;

@RestController
@RequestMapping("/partidos/voley")
public class ControladorPartidoVoley extends
		ControladorGenericoActualizar<PartidoVoley, Long> {
	
	@Autowired
	private ServicioPartidoVoley servicio;
	
	@Override
	@JsonView({Partido.Vistas.Resumen.class})
	@RequestMapping
	public Iterable<PartidoVoley> getPagina(@RequestParam Map<String, String> parametros) {
		return super.getPagina(parametros);
	}
	
	@Override
	@JsonView({Partido.Vistas.Resumen.class})
	@RequestMapping(params = { "sort" })
	public Iterable<PartidoVoley> getPagina(@RequestParam Map<String, String> parametros,
			Pageable pagina) {
		return super.getPagina(parametros, pagina);
	}
	
	@Override
	@JsonView({Partido.Vistas.Resumen.class})
	@RequestMapping("/{id}")
	public PartidoVoley get(@PathVariable("id") Long id) {
		return servicio.getItem(id);
	}
	
	@JsonView({Partido.Vistas.Resumen.class})
	@RequestMapping(method = RequestMethod.POST, value = "/iniciar/{id}")
	public PartidoVoley iniciarPartido(@PathVariable("id") Long id) {
		return servicio.iniciar(id);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value="/{id}")
	@JsonView({Partido.Vistas.Resumen.class})
	public PartidoVoley actualizar(@RequestBody PartidoVoley p, Long id) throws Exception {
		return servicioGenerico.actualizarItem(p);
	}

}
