package ar.edu.utn.frvm.juegos.persistencia;

import logisoft.generico.persistencia.RepositorioGenerico;

import org.springframework.stereotype.Repository;

import ar.edu.utn.frvm.juegos.dominio.partido.PartidoBasquet;
import ar.edu.utn.frvm.juegos.dominio.partido.PartidoFutbol;

@Repository
public interface RepositorioPartidoBasquet extends
		RepositorioGenerico<PartidoBasquet, Long> {

}
