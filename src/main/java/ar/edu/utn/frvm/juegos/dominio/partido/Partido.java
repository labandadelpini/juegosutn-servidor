package ar.edu.utn.frvm.juegos.dominio.partido;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.joda.time.Instant;
import org.joda.time.Interval;
import org.joda.time.Minutes;
import org.joda.time.format.PeriodFormat;

import com.fasterxml.jackson.annotation.JsonView;

import ar.edu.utn.frvm.juegos.dominio.Campeonato;
import ar.edu.utn.frvm.juegos.dominio.Equipo;
import ar.edu.utn.frvm.juegos.dominio.Fase;
import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartido;
import ar.edu.utn.frvm.juegos.dominio.estado.HistorialEstado;
import ar.edu.utn.frvm.juegos.dominio.grupo.Grupo;
import ar.edu.utn.frvm.juegos.soporte.UtilPeriodo;
import logisoft.generico.dominio.ObjetoDominio;
import lombok.Data;

@Entity
@Data
public class Partido extends ObjetoDominio {
	private int numeroPartido;
	private String horaEstimada;
	private Date fechaInicio;
	private String cancha;
	@ManyToOne
	private Equipo local;
	@ManyToOne
	private Equipo visita;
	@JsonView({ Partido.Vistas.Resumen.class })
	@ManyToOne
	private Grupo grupoLocal;
	@JsonView({ Partido.Vistas.Resumen.class })
	@ManyToOne
	private Grupo grupoVisita;
	@OneToMany(cascade = CascadeType.ALL)
	private List<HistorialEstado> estados = new ArrayList<HistorialEstado>();
	private Date ultimaActulizacion;
	@ManyToOne
	private EstadoPartido estado;
	
	public String getTiempoUltimoEvento() {
		if (estados.size() > 0) {
			Interval interval = new Interval(estados.get(estados.size() - 1).getFecha().getTime(),new Date().getTime());
			return UtilPeriodo.formatear(interval);
		}
		return "";
	}
	
	public String getTiempoAbreviado() {
		if (estados.size() > 0) {
			Interval interval = new Interval(estados.get(estados.size() - 1).getFecha().getTime(),new Date().getTime());
			return UtilPeriodo.formatearAbreviado(interval);
		}
		return "";
	}
	
//	public void setEstado(EstadoPartido e) {
//		this.estado = e;
//	}
//
//	public EstadoPartido getEstado() {
//		if (estados.size() > 0) {
//			return estados.get(estados.size() - 1).getEstado();
//		}
//		return null;
//	}

	public String getTipoObjeto() {
		return getClass().getCanonicalName();
	}

	public String getGrupo() {
		if (grupoLocal.getId() == grupoVisita.getId()) {
			return grupoLocal.getNombre();
		} else {
			return "Interzonal";
		}
	}

	public static class Vistas {
		public static class Resumen {

		}

		public static class Completa {

		}
	}
}
