package ar.edu.utn.frvm.juegos.web.grupo;

import logisoft.generico.web.genericos.ControladorGenericoActualizar;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.utn.frvm.juegos.dominio.Campeonato;

@RestController
@RequestMapping("/campeonatos")
public class ControladorCampeonato extends
		ControladorGenericoActualizar<Campeonato, Long> {

}
