package ar.edu.utn.frvm.juegos.dominio.estado;

import javax.persistence.Entity;

import logisoft.generico.dominio.ObjetoDominio;

@Entity
public class EstadoPartido extends ObjetoDominio {

	protected String nombre;
	protected int orden;
	protected String abreviatura;
	protected boolean tiempo;
	
	public EstadoPartido(){
		
	}

	public EstadoPartido(String nombre, int orden) {
		this.nombre = nombre;
		this.orden = orden;
	}
	
	public EstadoPartido(String nombre, int orden, String abreviatura, boolean tiempo) {
		this.nombre = nombre;
		this.orden = orden;
		this.abreviatura = abreviatura;
		this.tiempo = tiempo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}

	public boolean isTiempo() {
		return tiempo;
	}

	public void setTiempo(boolean tiempo) {
		this.tiempo = tiempo;
	}
	
	
	
}
