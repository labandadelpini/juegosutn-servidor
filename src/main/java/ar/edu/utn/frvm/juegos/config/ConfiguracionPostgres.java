package ar.edu.utn.frvm.juegos.config;

import javax.sql.DataSource;

import logisoft.generico.persistencia.util.RepositorioGenericoFactoryBean;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"logisoft.generico.persistencia","ar.edu.utn.frvm.juegos.persistencia"}, 
	entityManagerFactoryRef = "webEntityManager",
	repositoryFactoryBeanClass = RepositorioGenericoFactoryBean.class)
public class ConfiguracionPostgres {
	
	
	@Bean
	@Primary
	@ConfigurationProperties(prefix="spring.datasource")
	public DataSource webDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "webEntityManager")
	@Primary
    public LocalContainerEntityManagerFactoryBean webEntityManager() throws Throwable {
		return new EntityManagerFactoryBuilder(jpaVendorAdapter(jpaProperties()), jpaProperties().getProperties(), null)
				.dataSource(webDataSource())
				.packages("ar.edu.utn.frvm.juegos.dominio", "logisoft.generico.dominio")
				.persistenceUnit("webPersistenceUnit")
				.build();
    }
	
	@Bean(name = "webJpaProperties")
	@Primary
	@ConfigurationProperties(prefix="spring.jpa")
	public JpaProperties jpaProperties() {
		return new JpaProperties();
	}
	
	@Bean(name = "webJpaAdapter")
	@Primary
	public JpaVendorAdapter jpaVendorAdapter(@Qualifier("webJpaProperties") JpaProperties jpaProperties) {
		HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setShowSql(jpaProperties.isShowSql());
		adapter.setDatabase(jpaProperties.getDatabase());
		adapter.setDatabasePlatform(jpaProperties.getDatabasePlatform());
		adapter.setGenerateDdl(jpaProperties.isGenerateDdl());
		return adapter;
	}

}