package ar.edu.utn.frvm.juegos.soporte;

import org.joda.time.Interval;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

public class UtilPeriodo {

	private static PeriodFormatter minutos;
	private static PeriodFormatter minutosAbreviado;
	private static PeriodFormatter segundos;

	private static void validarFormateadores() {
		if (minutos == null) {
			minutos = new PeriodFormatterBuilder().appendPrefix("Hace ")
					.appendHours().appendSuffix(" hora y ", " horas y ").appendPrefix("")
					.appendMinutes().appendSuffix(" minuto", " minutos")
					.toFormatter();
		}

		if (minutosAbreviado == null) {
			minutosAbreviado = new PeriodFormatterBuilder().appendPrefix("")
					.appendMinutes().appendSuffix("'").toFormatter();
		}

		if (segundos == null) {
			segundos = new PeriodFormatterBuilder().appendPrefix("Hace ")
					.appendSeconds().appendSuffix(" segundo", " segundos")
					.toFormatter();
		}
	}

	public static String formatear(Interval intervalo) {
		validarFormateadores();
		if (intervalo.toDuration().getStandardMinutes() >= 1) {
			return minutos.print(intervalo.toPeriod());
		} else {
			return segundos.print(intervalo.toPeriod());
		}
	}

	public static String formatearAbreviado(Interval intervalo) {
		validarFormateadores();
		return minutosAbreviado.print(intervalo.toPeriod());
	}

}
