package ar.edu.utn.frvm.juegos.persistencia;

import org.springframework.stereotype.Repository;

import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoBasquet;
import logisoft.generico.persistencia.RepositorioGenerico;

@Repository
public interface RepositorioEstadoPartidoBasquet extends
		RepositorioGenerico<EstadoPartidoBasquet, Long> {
	public EstadoPartidoBasquet findByNombre(String nombre);
}
