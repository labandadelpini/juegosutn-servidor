package ar.edu.utn.frvm.juegos.dominio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import logisoft.generico.dominio.ObjetoDominio;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import ar.edu.utn.frvm.juegos.dominio.grupo.Grupo;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Fase extends ObjetoDominio {
	@NonNull
	private String nombre;
	@ManyToOne
	private Campeonato campeonato;
	@OneToMany
	private List<Grupo> grupos = new ArrayList<Grupo>();
	private int orden;
	
}
