package ar.edu.utn.frvm.juegos.persistencia;

import org.springframework.stereotype.Repository;

import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoFutbol;
import logisoft.generico.persistencia.RepositorioGenerico;

@Repository
public interface RepositorioEstadoPartidoFutbol extends
		RepositorioGenerico<EstadoPartidoFutbol, Long> {
	public EstadoPartidoFutbol findByNombre(String nombre);
}
