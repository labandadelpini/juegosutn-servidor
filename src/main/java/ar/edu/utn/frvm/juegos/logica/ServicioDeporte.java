package ar.edu.utn.frvm.juegos.logica;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import logisoft.generico.logica.ServicioGenerico;
import ar.edu.utn.frvm.juegos.dominio.Equipo;
import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoBasquet;
import ar.edu.utn.frvm.juegos.dominio.partido.Deporte;
import ar.edu.utn.frvm.juegos.persistencia.RepositorioDeporte;

@Service
public class ServicioDeporte extends ServicioGenerico<Deporte, Long> {
	
	@Autowired
	private RepositorioDeporte repositorio;
	
	@PostConstruct
	private void crearDeportes() {
		Set<Deporte> empresas = new HashSet<Deporte>();
		empresas.add(new Deporte("Futbol Masculino", 3,1,0));
		empresas.add(new Deporte("Futbol Femenino", 3,1,0));
		empresas.add(new Deporte("Voley Masculino", 2,0,1));
		empresas.add(new Deporte("Voley Femenino", 2,0,1));
		empresas.add(new Deporte("Basquet", 2,0,1));
		
		for (Deporte f : empresas) {
			if (repositorio.findByNombre(f.getNombre()) == null) {
				repositorio.save(f);
			}
		}
	}

}
