package ar.edu.utn.frvm.juegos.persistencia;

import org.springframework.stereotype.Repository;

import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoFutbol;
import ar.edu.utn.frvm.juegos.dominio.estado.EstadoPartidoVoley;
import logisoft.generico.persistencia.RepositorioGenerico;

@Repository
public interface RepositorioEstadoPartidoVoley extends
		RepositorioGenerico<EstadoPartidoVoley, Long> {
	public EstadoPartidoVoley findByNombre(String nombre);
}
