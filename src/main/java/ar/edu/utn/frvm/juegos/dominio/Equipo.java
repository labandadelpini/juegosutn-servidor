package ar.edu.utn.frvm.juegos.dominio;

import javax.persistence.Entity;

import logisoft.generico.dominio.ObjetoDominio;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
public class Equipo extends ObjetoDominio {
	@NonNull
	private String nombre;
	private String sigla;
	

}
